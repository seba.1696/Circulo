/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package circulo;

/**
 * Una clase para representar circulos situados sobre el plano. Cada circulo
 * queda determinado por su radio junto con las coordenadas de su centro.
 * @version 1.2, 24/12/04
 * @author seba
 */
public class Circulo {

    protected double x, y; //coordenadas del centro
    protected double r; //radio del circulo
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    /**
     * Crea un circulo a partir de su origen su radio.
     * @param x La coordenada x del centro del circulo.
     * @param y La coordenada y del centro del ciruclo.
     * @param r El radio del circulo. Debe ser mayor o igual a 0.
     */

    public Circulo(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }
    
    /**
     * Calculo del area del circulo.
     * @return El area (mayor o igual que 0) del circulo. 
     */

    public double Area() {
        return Math.PI * r * r;
    }
    
    /**
     * Indica si un punto esta dentro del circulo.
     * @param px componente x del punto.
     * @param py componente y del punto.
     * @return true si el punto esta dentro del circulo o false en otro caso
     */

    public boolean contiene(double px, double py) {
        /* Calculamos la distancia de (px, py) al centro del circulo (x, y),
        que se obtiene como la raiz cuadrada de (px-x)^2+(py-y)^2*/
        double d = Math.sqrt((px - x) * (px - x) + (py - y) * (py - y));
        // el circulo contiene el punto si d es menor o igual al radio
        return d <= r;
    }

}
